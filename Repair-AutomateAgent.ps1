<# 
Repair-AutomateAgent
-----
Attempts to repair a Connectwise Automate agent on the target computer.

Must be run on the target computer directly.

This script WILL NOT remove the computer from Automate; please double-check that no duplicate configurations are present after running the script.

Invocation through Screenconnect:
#timeout=6000000 
#maxlength=6000000 
"%windir%\system32\WindowsPowerShell\v1.0\powershell.exe" "Invoke-WebRequest -Uri 'INSERT SCRIPT URL HERE' -Outfile 'C:\Windows\Temp\Repair-AutomateAgent.ps1'; C:\Windows\Temp\Repair-AutomateAgent.ps1 -labtechServerUrl 'SERVER URL HERE' -LabtechPoshModuleUrl 'PoSH MODULE URL HERE' -labtechServerPw 'LT SERVER PASS HERE'"
#>

param (
    [Parameter(Mandatory)]
    $labtechServerUrl,
    $labtechPoshModuleUrl = "https://gitlab.com/jwguidryvital/gavsto-labtech-powershell-module/-/raw/master/LabTech.psm1",
    [Parameter(Mandatory)]
    $labtechServerPw
)

# Pull the Labtech powershell module
Invoke-Expression (new-object Net.WebClient).DownloadString($($labtechPoshModuleUrl))

# Get the current Labtech configuration
$currentConfig = Get-LTServiceInfo

# Uninstall Labtech
Uninstall-LTService -Server $labtechServerUrl

# If a location ID was present, try to reinstall the agent for that location; otherwise, install at the default location
if ($currentConfig.LocationID) {
    Install-LTService -Server $labtechServerUrl -ServerPassword $labtechServerPw -LocationID $currentConfig.LocationID -ErrorAction SilentlyContinue
} else {
    Install-LTService -Server $labtechServerUrl -ServerPassword $labtechServerPw -LocationID 1 -ErrorAction SilentlyContinue
}

